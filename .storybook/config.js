import { configure } from '@storybook/react';
// config.js
import { setDefaults } from '@storybook/addon-info';

// addon-info
setDefaults({
    header: false, // Toggles display of header with component name and description
    inline: true,
});

function loadStories() {
  require('../src/stories');
}
// addDecorator(withInfo);
configure(loadStories, module);
