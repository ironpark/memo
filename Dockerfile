FROM kkarczmarczyk/node-yarn
MAINTAINER ironpark <cjfdhksaos@gmail.com>

WORKDIR /app
#RUN git clone https://github.com/dramancompany/memoapp-api
#RUN git clone https://ironpark@bitbucket.org/ironpark/memo.git
#
#RUN yarn --cwd memo
#RUN yarn --cwd memoapp-api
#RUN rm memo/.env.production
#RUN echo "REACT_APP_API_URL=http://localhost:3001" >> memo/.env.production
COPY build /app/build
RUN yarn global add serve
#RUN yarn --cwd memo build

EXPOSE 3001
#CMD serve memo/build -l 3001
CMD serve build -l 3001

