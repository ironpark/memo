
# Memo App
리액트를 기반으로 만들어진 
다양한 메모를 라벨을 이용하여 분류,저장,수정 할 수 있는 웹 어플리케이션.

## Project Structure

```cmd
.
├── README.md
└── src
    ├── App.css
    ├── App.js
    ├── App.scss
    ├── App.test.js
    ├── components                    # 단일 컴포넌트
    │   ├── Header.js
    │   ├── LabelAutoSuggestInput.js    - 라벨 입력 자동완성 컴포넌트
    │   ├── LabelCreateDialog.js        - 라벨 생성 다이얼로그
    │   ├── LabelMenu.js                - 좌측 메인 메뉴
    │   ├── MemoListItem.js 
    │   └── MemoListToolbar.js
    ├── container                     # 컴포넌트 조합 컨테이너
    │   ├── MemoList.js                 - 메모 리스트 컨테이너
    │   └── MemoViewer.js               - 메모 수정/추가 컨테이너
    ├── index.css
    ├── index.js
    ├── lib                           # 프로젝트에 사용되는 커스텀 라이브러리들
    │   ├── api-helper.js               - api class 를 보다 쉽게 활용하기 위한 helper class
    │   ├── api.js                      - axios 기반 api 통신 class 
    │   └── util.js                     - 기타 유용한 유틸리티 모음
    ├── registerServiceWorker.js
    └── stories                       # 컴포넌트 문서화를 위한 storybook
        └── index.js


```

## Dependencies

- **Framework**
    - *react*: "^16.5.1"
    - *react-dom*: "^16.5.1"
- **UI-Components**
    -  material-ui (https://material-ui.com/)
        - @material-ui/core: ^3.0.3
        - @material-ui/icons: ^3.0.1
    - material-ui-chip-input
    - react-autosugges
- **Unit-Test**
    - jest-cli: ^23.6.0
- **ETC**
    - normalize.css: ^8.0.0
    - react-router-dom: ^4.3.1,
    - react-scripts: 1.1.5 
    *(build script)*
    - styled-components: ^3.4.6
    - axios
    - lodash
    - date-fns

## Build & Run
서비스 배포시 .env.production 파일내부의 api endpoin 수정필요
### install dependencies
```bash
npm install
```
### build only
```bash
npm run build
```
### serve only
```bash
npm run serve   
```
### build & serve
```bash
npm run build2serve   
```
### debug
```bash
npm run start   
```
   
> 모든 npm 명령어는 yarn 으로 대체 가능합니다.

```bash
yarn
yarn build
```
## Docker
```bash
npm install
npm run build
docker build -t memo-app .   
docker run -d -p 3001:3001 memo-app-v2
```

## StoryBook
```bash
yarn storybook
```

## Basic Requirements

- [ x ] - SPA(Single Page Application) 형태로 동작해야 하며
처음 페이지 로드와 새로고침을 할 때 외에는 전체 페이지 reload는 일어나지 않아야 합니다.
- [ x ] - Text로만 이루어진 메모를 저장할 수 있습니다. (별도의 styling 없음)
- [ x ] - 메모는 제목, 내용, 최종 수정일, 생성일을 가지고 있습니다.
- [ x ] - 라벨로 메모들을 분류할 수 있어야 합니다.
- [ x ] - 메모를 추가/수정/삭제 할 수 있어야 합니다.
- [ x ] - 하나의 메모에는 여러 라벨을 지정할 수 있으며, 하나의 라벨에도 여러 메모를 지정할 수 있습니다.
- [ x ] - 라벨에 해당하는 메모가 하나도 없을 경우에는 메모가 없다는 화면을 표시해줘야 합니다.
- [ x ] - 특정 라벨과 메모를 조회한 상태에서 새로고침을 할 경우, 기존의 화면을 보여줘야 합니다.
- [ x ] - 라벨과 메모의 조회 순서에 따라 페이지 뒤로가기를 통하여 조회 할 수 있어야 합니다.

## Improvements
체크가 안된것은 아직 미구현.

- [  ] Label 을 구분하기 쉽게 하기 위해 색상 지정 기능.
- [  ] 북마크, 미분류 메뉴
- [  ] 초기화, 다중 선택 + 라벨 삭제 등을 지원 하기 위한 관리페이지 추가
- [ x ] 기존 기획에서 메모생성 버튼을 상단으로 변경.
- [ x ] 화면이 크기때문에 plain text 보다는 style 이 가미된 위지윅 에디터로 변경.
- [  ] 제목,내용,라벨 등으로 검색 할 수 있는 검색기능
- [  ] 일정시간이 지났을때 오토세이브(임시저장)

## TODO
- docker 등 배포관련 세팅
- 서버측 오류에대한 정의 필요.
- 메모리스트에 라벨을 보여주기위한 server api 수정 
    - 현재  api는  해당기능을 구현하기에는 비효율적
    
- 애니메이션효과 (리스트 아이템 추가, 수정완료시 피드백을 주기 위한.)
- 로그인, 계정관리
- 이미지 / 동영상 업로드

