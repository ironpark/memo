/*eslint no-extend-native: ["error", { "exceptions": ["String"] }]*/

import React from 'react';
import {BrowserRouter as Router, Link, Redirect, withRouter} from "react-router-dom";
import {Route} from 'react-router-dom';
import styled from 'styled-components'

import './App.css';
import _ from "lodash";
import MainMenu from "./components/LabelMenu";
import LabelCreateDialog from "./components/LabelCreateDialog"
import Header from "./components/Header";
import Button from "@material-ui/core/Button/Button";
import AddIcon from '@material-ui/icons/Add'

import Divider from "@material-ui/core/Divider/Divider";
import ListItem from "@material-ui/core/ListItem/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon/ListItemIcon";
import Icon from "@material-ui/core/Icon/Icon";
import ListItemText from "@material-ui/core/ListItemText/ListItemText";

import Grid from "@material-ui/core/Grid/Grid";
import MemoList from "./container/MemoList";
import MemoViewer from "./container/MemoViewer";
import MemoApi from './lib/api'
import {PropsRoute, customSetState} from "./lib/util";

//extend String
String.prototype.replaceAll = function (search, replacement) {
    let target = this;
    return target.split(search).join(replacement);
};

const Root = styled.div`
            flex-grow: 1;
            height: 100vh;
            z-index: 1;
            overflow: hidden;
            position: relative;
            display: flex;
            Header{
               z-index: 1201; //AppBar z-index + 1
            }
    `;

const MenuItem = (props) => {
    return (
        <ListItem button dense onClick={props.onClick}>
            <ListItemIcon>
                <Icon className={props.icon}>{props.icon}</Icon>
            </ListItemIcon>
            <ListItemText primary={props.title}/>
        </ListItem>
    );
};

const Container = styled(Grid)`
      padding-top:65px;
      height: 100%;
    `;

const api = new MemoApi();

class App extends React.Component {
    constructor() {
        super();
        customSetState(this);
    }

    state = {
        dialog: {
            value: "",
            open: false,
        },
        menu: {
            open: true,
            labels: [],
        },
        // labels => { memo_id : [{label_id,label_title} ...] }
        // 검색용 Map.
        labels: new Map(),
        memoList: [],
    };
    // label create dialog
    handleChange = (event) => {
        this.setState({
            dialog: {
                value: event.target.value,
            }
        });
    };

    openDialog = () => {
        this.setState({dialog: {open: true, value: ''}})
    };

    onClose = () => {
        this.setState({dialog: {open: false}})
    };

    //========== Memu
    drawerState = () => {
        // value inversion
        this.setState({menu: {fn$open: (e) => !e}})
    };

    onOk = () => {
        // if(this.state.dialog.value)
        api.Label.Create(this.state.dialog.value.replaceAll(' ', '-'))
            .then(() => {
                return api.Label.List()
            })
            .then((result) => {
                this.setState(
                    {
                        menu: {
                            labels: result,
                            open: this.state.menu.open,
                        },
                        dialog: {
                            open: false
                        }
                    });
            });
    };
    //=========== Memo CRUD Events
    onPost = (ee) => {
        let labelId = [];
        ee.labels.map(i => {
            const id = this.labelIdFromTitle(i);
            if (id) {
                labelId.push(id);
            }
        });

        api.Memo.Create({title: ee.title, content: ee.content}, labelId).then((e) => {
            this.updateLabelsAndMemos();
            if(this.props.location.pathname.indexOf('create') !== -1){
                this.props.history.push(
                    this.props.location.pathname.replace('create',e._id)
                )
            }
        })
    };

    onDelete = (ee) => {
        if (this.state.labels.has(ee.id)) {
            let labels = this.state.labels.get(ee.id);
            labels.map(i => {
                api.Label.DeleteMemofromLabel(i.id, ee.id).then();
            })
        }
        api.Memo.Delete(ee.id).then(() => {
            this.updateLabelsAndMemos();
        });
        this.props.history.goBack();
    };

    onEdit = (ee) => {
        api.Memo.Update(ee.id, {title: ee.title, content: ee.content}).then(() => {
            this.updateLabelsAndMemos()
            // this.props.history.push("/")
        })
    };

    //========== Utils Func
    labelIdFromTitle = (title) => {
        let ids = [];
        this.state.menu.labels.map((i) => {
            if (i.title === title) {
                ids.push(i);
            }
        });

        if (ids.length !== 0) {
            return ids[0]._id;
        }
        return null;
    };

    //========== Refresh Data
    updateMemos = () => {
        api.Memo.List().then((result) => {
            // console.log("test",result);
            result = result.map(i => {
                if (this.state.labels.has(i._id)) {
                    i["labels"] = this.state.labels.get(i._id)
                }
                return i
            });
            this.setState({
                memoList: result
            });
        });
    };

    updateLabelsAndMemos = () => {
        //get labels from api server then update state
        api.Label.List().then((labels) => {
            // { memo_id : [{label_id,label_title} ...] }
            let map = new Map();
            labels.map((label) => {
                if(label.memos.length !== 0) {
                    _.forEach(label.memos, (memoId, key) => {
                        if (map.has(memoId)) {
                            let lb = map.get(memoId);
                            lb.push({id: label['_id'], title: label['title']});
                            map.set(memoId, lb)
                        } else {
                            map.set(memoId, [{id: label['_id'], title: label['title']}])
                        }
                    })
                }else{
                    // let unknown = map.get('unknown')
                    // unknown.push()
                }
            });

            // labelIdFromTitle
            this.setState({
                labels: map,
                menu: {labels: labels}
            });

            this.updateMemos();
        });
    };

    componentDidMount() {
        this.updateLabelsAndMemos();
    }

    render() {
        let menu = this.state.menu;
        let dialog = this.state.dialog;
        return (
            <Router>
                <Root>
                    {/* Redirect for mainpage -> total */}
                    <Redirect exact Path="/" to="/label/total" />
                    <Header onClick={this.drawerState}/>

                    <MainMenu open={this.state.menu.open}>
                        <Link to={"/label/total/create"} style={{textDecoration: "none"}}>
                            <Button variant="extendedFab" color="secondary" size={"medium"} style={{margin: 5}}>
                                <AddIcon/>
                                새로운 메모
                            </Button>
                        </Link>
                        <Link to={"/label/total"} style={{textDecoration: "none"}}>
                            <MenuItem title={`전체 메모함 ( ${this.state.labels.size} )`} icon={'all_inbox_icon'}/>
                        </Link>
                        <Link to={"/label/bookmark"} style={{textDecoration: "none"}}>
                            <MenuItem title={"북마크"} icon={'bookmark'}/>
                        </Link>
                        <Link to={"/label/unknown"} style={{textDecoration: "none"}}>
                            <MenuItem title={"미분류"} icon={'help_outline'}/>
                        </Link>
                        <Divider style={{visibility: menu.labels.length !== 0 ? 'visible' : 'hidden'}}/>

                        {
                            menu.labels.map(
                                label =>
                                    <Link key={label._id} to={"/label/" + label._id} style={{textDecoration: "none"}}>
                                        <MenuItem title={`${label.title} ( ${label.memos.length} )`} icon={"label"}/>
                                    </Link>
                            )
                        }

                        <Divider/>
                        <MenuItem title={"새로운 라벨 추가"} icon={'add'} onClick={(e) => this.openDialog(e)}/>
                        <MenuItem title={"라벨 설정"} icon={'settings'}/>
                    </MainMenu>
                    {/*<Switch>*/}
                    <Container container spacing={0}>
                        <Grid key={1} item xs={5}>
                            {/*==== Memo List ====*/}
                            <PropsRoute exact path="/label/:labelId/:memoId" component={MemoList}
                                        items={this.state.memoList} labels={this.state.labels}
                                        labelMap={this.state.labels}/>
                            <PropsRoute exact path="/label/:labelId" component={MemoList} items={this.state.memoList}
                                        labels={this.state.labels} labelMap={this.state.labels}/>
                            <PropsRoute exact path="/label" component={MemoList} items={this.state.memoList}
                                        labels={this.state.labels} labelMap={this.state.labels}/>
                            <PropsRoute exact path="/label/total" component={MemoList} items={this.state.memoList}
                                        labels={this.state.labels} labelMap={this.state.labels}/>
                        </Grid>
                        <Grid key={2} item xs={7}>
                            {/*{this.props.location.}*/}
                            {/*==== Viewer || Editor ====*/}
                            <PropsRoute path="/label/:labelId/:memoId" component={MemoViewer}
                                        labels={this.state.menu.labels}
                                        labelMap={this.state.labels}
                                        onEdit={this.onEdit}
                                        onDelete={this.onDelete}
                                        onPost={this.onPost}/>
                        </Grid>
                    </Container>
                    {/*</Switch>*/}
                    {/*==== Main Contents ====*/}


                    {/*==== dialogs ====*/}
                    <LabelCreateDialog open={dialog.open} onClose={this.onClose} onOk={this.onOk}
                                       value={dialog.value}
                                       onChange={this.handleChange}/>
                </Root>
            </Router>
        );
    }
}

const warpApp = withRouter(App);

class Main extends React.Component {
    render() {
        return (
            <Router>
                <Route Path="/" component={warpApp}/>
            </Router>
        )
    }
}

export default Main;
