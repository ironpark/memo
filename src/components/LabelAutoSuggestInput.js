import React, {Component} from 'react';
import Autosuggest from 'react-autosuggest'
import match from 'autosuggest-highlight/match'
import parse from 'autosuggest-highlight/parse'
import withStyles from "@material-ui/core/styles/withStyles";
import ChipInput from 'material-ui-chip-input'
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import Paper from "@material-ui/core/Paper/Paper";

function renderInput (inputProps) {
    const { classes, autoFocus, value, onChange, onAdd, onDelete, chips, ref, ...other } = inputProps

    return (
        <ChipInput
            clearInputValueOnChange
            onUpdateInput={onChange}
            onAdd={onAdd}
            onDelete={onDelete}
            value={chips}
            inputRef={ref}
            {...other}
        />
    )
}

function renderSuggestion (suggestion, { query, isHighlighted }) {
    const matches = match(suggestion.name, query)
    const parts = parse(suggestion.name, matches)

    return (

        <MenuItem
            selected={isHighlighted}
            component='div'
            onMouseDown={(e) => e.preventDefault()} // prevent the click causing the input to be blurred
        >
            <div>
                {parts.map((part, index) => {
                    return part.highlight ? (
                        <span key={String(index)} style={{ fontWeight: 500,color:"#eb3f6a"}}>
                            {part.text}
                        </span>
                        ) : (
                        <strong key={String(index)} style={{ fontWeight: 300 }}>
                            {part.text}
                        </strong>
                        )
                })}
            </div>
        </MenuItem>
    )
}

function renderSuggestionsContainer (options) {
    const { containerProps, children } = options;

    return (
        <Paper {...containerProps} square style={{background:"#fff",zIndex:2}}>
            {children}
        </Paper>
    )
}

function getSuggestionValue (suggestion) {
    return suggestion.name
}

function getSuggestions (value,suggestions) {
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;
    let count = 0;

    return (inputLength === 0 )
        ? []
        : suggestions.filter(suggestion => {
            const keep =
                count < 5 && suggestion.name.toLowerCase().slice(0, inputLength) === inputValue;

            if (keep) {
                count += 1
            }

            return keep
        })
}

const styles = theme => ({
    container: {
        flexGrow: 1,
        position: 'relative',

    },
    suggestionsContainerOpen: {
        position: 'absolute',
        marginTop: theme.spacing.unit,
        marginBottom: theme.spacing.unit * 3,
        left: 0,
        right: 0
    },
    suggestion: {
        display: 'block',
    },
    suggestionsList: {
        margin: 0,
        padding: 0,
        listStyleType: 'none',
    },
    textField: {
        width: '100%'
    }
});

class LabelAutoSuggestInput extends Component {
    state = {
        // value: '',
        suggestions: [],
        value: [],
        textFieldInput: ''
    };

    handleSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            suggestions: getSuggestions(value,this.props.suggestions)
        })
    };

    handleSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        })
    };

    handletextFieldInputChange = (event, { newValue }) => {
        this.setState({
            textFieldInput: newValue
        })

    };

    handleAddChip (chip) {
        this.setState({
            value: [...this.state.value, chip],
            textFieldInput: ''
        });
        this.props.onValueChange([...this.state.value, chip])
    }

    handleDeleteChip (chip, index) {
        let temp = this.state.value;
        temp.splice(index, 1);
        this.setState({value: temp});
        this.props.onValueChange(temp)
    }

    componentDidMount(){
        this.setState({value: this.props.chipValue});
    }
    // param 변경에따른 업데이트 .
    componentDidUpdate=(prevProps, prevState)=> {
        if (this.props.chipValue !== prevProps.chipValue) {
            this.setState({value: this.props.chipValue});
        }
    };

    render () {
        const { classes, ...rest } = this.props;

        return (
            <Autosuggest
                theme={{
                    container: classes.container,
                    suggestionsContainerOpen: classes.suggestionsContainerOpen,
                    suggestionsList: classes.suggestionsList,
                    suggestion: classes.suggestion
                }}
                renderInputComponent={renderInput}
                suggestions={this.state.suggestions}
                onSuggestionsFetchRequested={this.handleSuggestionsFetchRequested}
                onSuggestionsClearRequested={this.handleSuggestionsClearRequested}
                renderSuggestionsContainer={renderSuggestionsContainer}
                getSuggestionValue={getSuggestionValue}
                renderSuggestion={renderSuggestion}
                onSuggestionSelected={(e, {suggestionValue}) => { this.handleAddChip(suggestionValue); e.preventDefault() }}
                focusInputOnSuggestionClick={false}
                inputProps={{
                    classes,
                    chips: this.state.value,
                    onChange: this.handletextFieldInputChange,
                    value: this.state.textFieldInput,
                    onAdd: (chip) => this.handleAddChip(chip),
                    onDelete: (chip, index) => this.handleDeleteChip(chip, index),
                    ...rest
                }}
            />
        )
    }
}

// LabelAutoSuggestInput.propTypes = {};
export default withStyles(styles)(LabelAutoSuggestInput);
