import React from 'react';
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import TextField from "@material-ui/core/TextField/TextField";

import DialogContentText from "@material-ui/core/DialogContentText/DialogContentText";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import Dialog from "@material-ui/core/Dialog/Dialog";


const LabelCreateDialog = props => {
    return (
        <Dialog
            open={props.open}
            onClose={props.onClose}>

            <DialogTitle>새로운 라벨 등록</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    생성하실 라벨의 이름을 적어주세요.
                </DialogContentText>
                <TextField
                    value={props.value} onChange={props.onChange}
                    autoFocus
                    error={props.error}
                    errorText={props.errorText}
                    margin="dense"
                    id="name"
                    type="email"
                    fullWidth/>
            </DialogContent>
            <DialogActions>
                <Button onClick={props.onClose} color="primary">
                    취소
                </Button>
                <Button onClick={props.onOk} color="primary">
                    등록
                </Button>
            </DialogActions>
        </Dialog>
    );
};

// Nonce Work 2G
// Nonce Work 5G

// LabelCreateDialog.propTypes = {
//
// };

export default LabelCreateDialog;
