import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';

const drawerWidth = 240;

const styles = theme => ({
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
        paddingTop: 65,
    },
});

const LabelMenu = (props) => {
  const { classes } = props;
  return (
    <Drawer
        open={props.open}
        variant="persistent"
        classes={{ paper : classes.drawerPaper}}>
        {props.children}
    </Drawer>
  );
};

LabelMenu.propTypes = {
    open:PropTypes.bool.isRequired,
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LabelMenu);
