
import React from 'react';
import Checkbox from "@material-ui/core/Checkbox/Checkbox";
import IconButton from "@material-ui/core/IconButton/IconButton";
import Bookmark from "@material-ui/icons/Bookmark";
import styled from 'styled-components'

const Chip = styled.span`
  color:#ffffff;
  font-size: 12px;
  height: 20px;
  padding-left:2px;
  padding-right:2px;
  background:#f5466b;
  border-radius: 2px;
  margin:1px;
`;

const Root = styled.div`
  display: -webkit-flex;
  display: flex;
  background: #fff;
  //border: solid 1px #aaa8ac;
  border-left : solid #a4a4a4 0px;
  border-right : solid #a4a4a4 0px;
  border-bottom : solid #a4a4a4 1px;
  //div:not(:first-child) {
  //  border-left: solid #a4a4a4 1px;
  //}
  padding-bottom: 10px;
  padding-top: 10px;
  .title{
    font-size: 15pt;
    text-overflow: ellipsis;
    overflow: hidden; 
    max-width: 290px;
  }
  .summary{
    font-size: 12pt;
    color: #5e5e5e;
    text-overflow: ellipsis;
    overflow: hidden; 
    max-width: 290px;
  }
  cursor:pointer;
`;

const Contents = styled.div`
  padding: 0px;
  flex: 2;
`;

const Meta = styled.div`
  width: 200px;
  padding: 0px;
`;



const MemoListItem = (props) => {
    return (
        <React.Fragment>
            <Root>
                <IconButton aria-label="Comments">
                    <Checkbox
                        checked={props.isChecked}
                        tabIndex={-1}
                        disableRipple
                        onClick={props.onClick}
                    />
                </IconButton>
                <IconButton aria-label="Comments" onClick={props.onClick}>
                    <Bookmark style={{color:props.isBookmarked ? "#e54164":"#bfbfbf"}}/>
                </IconButton>
                <Contents onClick={props.onClick}>
                    <div className={"title"}>{props.title}</div>
                    <div className={"summary"}>{props.summary}</div>
                </Contents>
                <Meta onClick={props.onClick}>
                    <div>
                        {props.date}
                    </div>
                    <div style={{overflowX: "hidden",width:200, whiteSpace:"nowrap"}}>
                        {props.labels?
                            (props.labels.map((value,index) => (
                            <Chip key={index}>{value.title}</Chip>
                            ))):(<span/>)}
                    </div>
                </Meta>
            </Root>
        </React.Fragment>
    );
};

// bat | 0x

// poly ->

MemoListItem.propTypes = {};

export default MemoListItem;