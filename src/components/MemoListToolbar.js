import React from "react";
import Checkbox from "@material-ui/core/Checkbox/Checkbox";
import IconButton from "@material-ui/core/IconButton/IconButton";
import BookmarkIcon from "@material-ui/icons/Bookmark";
import LabelIcon from '@material-ui/icons/Label'

import styled from 'styled-components'
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import Select from "@material-ui/core/Select/Select";
import DeleteIcon from "@material-ui/icons/Delete"

const Root = styled.div`
  display: -webkit-flex;
  display: flex;
  background: #fff;
  border: solid 1px #aaa8ac;
  border-left : solid #a4a4a4 0px;
  border-right : solid #a4a4a4 0px;
  div:not(:first-child) {
    border-left: solid #a4a4a4 1px;
  }
  .select:before { border: solid 0px;}
  .select{
    padding-left: 10px;
  }
`;

const MyItem = styled.div`
  padding: 0px;
`;

const MemoListToobar = (props) => {
    return (
        <React.Fragment>
            <Root>
                <MyItem>
                    <IconButton aria-label="Comments">
                        <Checkbox
                            checked={props.check}
                            tabIndex={-1}
                            disableRipple
                        />
                    </IconButton>
                </MyItem>
                <MyItem>
                    <IconButton aria-label="Comments">
                        <BookmarkIcon/>
                    </IconButton>
                </MyItem>
                <MyItem>
                    <IconButton aria-label="Comments">
                        <DeleteIcon/>
                    </IconButton>
                </MyItem>
                {/*<MyItem>*/}
                <Select
                    className={'select'}
                    value={props.sortType}
                    onChange={props.handleChange}
                    inputProps={{
                        name: 'age',
                        id: 'age-simple',
                    }}
                    style={
                        {
                            display: 'flex',
                            alignItems: 'center',
                            borderBottom:'',
                        }
                    }
                >
                    <MenuItem value={0}>작성일 오름차순</MenuItem>
                    <MenuItem value={1}>작성일 내림차순</MenuItem>
                    <MenuItem value={2}>수정일 오름차순</MenuItem>
                    <MenuItem value={3}>수정일 내림차순</MenuItem>
                </Select>
                {/*</MyItem>*/}
                <MyItem style={{
                    flex: 1,
                    display: 'flex',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    textAlign: "right",
                    verticalAlign: "middle"
                }}>
                    <div style={{fontSize: '17px', marginLeft: "10px"}}>{props.title}</div>
                </MyItem>
            </Root>
        </React.Fragment>
    )
};

MemoListToobar.propTypes = {};

MemoListToobar.defaultProps = {};

export default MemoListToobar;