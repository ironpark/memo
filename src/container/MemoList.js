// @flow
import React from 'react';
import PropTypes from 'prop-types';
import List from '@material-ui/core/List';
import MemoListItem from "../components/MemoListItem";
import MemoListToobar from "../components/MemoListToolbar";
import styled from "styled-components";
import {format} from 'date-fns'
import MemoApi from '../lib/api'

const Root = styled.div`
  height: 100%;
  background: #ffffff;
  padding-top: 0;
  padding-bottom: 0;
  //border: solid 1px #ccc;
  border-right: solid 1px #ccc;
  .list{
    padding-top: 0;
    padding-bottom: 0;
    overflow: scroll;
    height: 100%;
    
  }
  h1{
    font-size: 20pt;
    margin: 10px;
    padding: 0px;
  }
`;

const api = new MemoApi();

const sortFunc = (num) =>{
    switch (num) {
        case 0: //create asc
            return(a,b)=>{
                if ( a.createdAt < b.createdAt ) {
                    return -1;
                }
                if ( a.createdAt > b.createdAt ) {
                    return 1;
                }
                return 0;
            };
        case 1: //create desc
            return(a,b)=>{
                if ( a.createdAt > b.createdAt ) {
                    return -1;
                }
                if ( a.createdAt < b.createdAt ) {
                    return 1;
                }
                return 0;
            };
        case 2: //updated asc
            return(a,b)=>{
                if ( a.updatedAt < b.updatedAt ) {
                    return -1;
                }
                if ( a.updatedAt > b.updatedAt ) {
                    return 1;
                }
                return 0;
            };
        case 3: //updated desc
            return(a,b)=>{
                if ( a.updatedAt > b.updatedAt ) {
                    return -1;
                }
                if ( a.updatedAt < b.updatedAt ) {
                    return 1;
                }
                return 0;
            };
    }
};
class MemoList extends React.Component {
    state = {
        items:[],
        error:false,
        errorText:'',
        sortType:3,
    };

    // param 변경에따른 업데이트 .
    componentDidUpdate=(prevProps, prevState)=> {
        if (this.props.match.params.labelId !== prevProps.match.params.labelId ||
            this.props.labels !== prevProps.labels

        ) {
            this.updateMemoList();
        }
    };

    updateMemoList=()=>{
        if(!this.props.match)return;
        const params = this.props.match.params;


        switch (params.labelId ) {
            case "total":
            case "create":
            case "bookmark":
            case "unknown":
                {
                    api.Memo.List().then(r=>{
                        if(r.length <= 0) {
                            this.setState({errorText:"메모가 없습니다.",error:true});
                        }else {
                            this.setState({items: r.sort(sortFunc(this.state.sortType)),error: false});
                        }
                    }).catch((e)=>{
                        this.setState({errorText:"서버에서 리스트를 불러 올 수 없습니다.",error:true,items:[]});
                    });
                }
            break;
            default: {
                if (params.labelId) {
                    api.Memo.ListFromLabel(params.labelId).then(r => {
                        if (r.length === 0) {
                            this.setState({errorText: "해당 라벨이 지정된 메모가 없습니다.", error: true, items: []});
                        } else {
                            this.setState({items: r.sort(sortFunc(this.state.sortType)),error: false});
                            console.log(r);
                        }
                    }).catch(e => {
                        this.setState({errorText: "서버에서 리스트를 불러 올 수 없습니다.", error: true, items: []});
                    });
                }
            }
        }

    };
    sortTypeChange=(e)=>{
        this.setState({sortType:e.target.value});
        console.log(this.state.items);//this.state.items
        this.setState({items: this.state.items.concat().sort(sortFunc(e.target.value))});
    };
    // // componentWillUpdate
    componentDidMount=()=>{
        this.updateMemoList();
    };

    render() {
        const props = this.props;

        const labelId = this.props.match.params.labelId;
        return (
            <Root>
                <MemoListToobar title={""} sortType={this.state.sortType} handleChange={this.sortTypeChange}/>
                <List className={"list"}>

                    {this.state.items.map((value,index) => (
                        <MemoListItem
                            key={index}
                            onClick={()=>{this.props.history.push(`/label/${labelId}/${value._id}`) }}
                            title={value.title}
                            summary={value.content.replace(/<(.|\n)*?>/g, '')}
                            date={format(value.createdAt,'YYYY-MM-DD hh:mm:ss')}
                            labels = {this.props.labelMap.get(value._id)}
                            isChecked = {value.check}
                            isBookmarked = {value.bookmark}
                        />
                    ))}
                    {this.state.error?(<h2 style={{padding:10}}>{this.state.errorText}</h2>):(<span/>)}
                    {/*dummy for scroll..*/}
                    <div style={{paddingBottom:100}}/>
                </List>
            </Root>
        );
    }
}

MemoList.defaultProps = {
    items: [],
};

MemoList.propTypes = {
    items:PropTypes.arrayOf(PropTypes.object),
};


export default MemoList;
