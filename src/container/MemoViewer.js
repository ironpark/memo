import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import 'react-quill/dist/quill.snow.css';
import ReactQuill from 'react-quill';
import styled from "styled-components";
import TextField from "@material-ui/core/TextField/TextField";
import Button from "@material-ui/core/Button/Button";
import LabelAutoSuggestInput from "../components/LabelAutoSuggestInput";
import MemoApi from "../lib/api";
import Grid from "@material-ui/core/Grid/Grid";


const drawerWidth = 230;
const styles = theme => ({
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
    },
});

const Root = styled.div`
   position: relative;
`;
const api = new MemoApi();
// const editor = new Editor();
class MemoList extends React.Component {
    constructor(props) {
        super(props);
        this.editor = null;
    }

    state = {
        edit:false,
        content: '',
        labels:[],
        title:'',
    };

    updateMemo=()=>{
        const params = this.props.match.params;
        api.Memo.Get(params.memoId).then((r)=>{
            this.setState({title:r.title,content:r.content});
            let labels = this.props.labelMap.get(params.memoId);
            if(labels){
                this.setState({labels:labels.map(i=>{
                    return i.title
                    })});
            }else{
                this.setState({labels:[]});
            }

        }).catch((e)=>{

        })
    };

    // param 변경에따른 업데이트 .
    componentDidUpdate=(prevProps, prevState)=> {
        if (this.props.labels !== prevProps.labels ||
            this.props.location.pathname !== prevProps.location.pathname) {
            this.updateMemo();
            if(this.props.location.pathname.indexOf('create')!== -1) {
                this.setState({ edit:false,
                    content: '',
                    labels:[],
                    title:''
                });
            }else{
                this.setState({edit: true});
            }
        }
    };

    componentDidMount() {
        const params = this.props.match.params;

        if(params.memoId){
            this.setState({edit:true});
            api.Memo.Get(this.props.match.params.memoId).then((r)=>{
                this.updateMemo()
            })
        }
        if(this.props.location.pathname.indexOf('create')!== -1) {
            this.setState({edit: false});
        }else{
            this.setState({edit: true});
        }


        // labels={this.state.labels}
    }

    handleChangeTitle=(event) => {
        this.setState({title: event.target.value})
    };

    handleChangeLabels = (value) => {
        this.setState({labels: value})
    };

    handleChangeContents = (value) =>{
        this.setState({content: value})
    };

    onPost = (value) => {
        this.props.onPost({
            title:this.state.title,
            content: this.state.content,
            labels:this.state.labels,
        })
    };

    onEdit = (value) => {
        this.props.onEdit({
            id:this.props.match.params.memoId,
            title:this.state.title,
            content: this.state.content,
            labels:this.state.labels,
        })
    };

    onDelete = (value) => {
        this.props.onDelete({
            id:this.props.match.params.memoId,
        });
        this.setState({
            edit:false,
            content: '',
            labels:[],
            title:'',
        });
    };

    render() {
        const suggests = this.props.labels.map((i) => {
            return {name: i.title}
        });

        return (
            <Root className={this.props.classes.root} elevation={0} style={{margin: 8}}>
                <TextField
                    label="제목"
                    placeholder="제목을 입력하세요"
                    error ={this.state.title.length !== 0 ? false : true }
                    helperText={this.state.title.length !== 0 ? "" : "제목은 반드시 입력되어야 합니다."}
                    fullWidth
                    margin="normal"
                    variant="filled"
                    value={this.state.title}
                    onChange={this.handleChangeTitle}
                    InputLabelProps={{
                        shrink: true,
                    }}
                />

                <LabelAutoSuggestInput label="라벨"
                                       placeholder="라벨을 입력하세요"
                                       margin="normal"
                                       fullWidth
                                       variant="filled"
                                       InputLabelProps={{
                                           shrink: true,
                                       }}
                                       chipValue={this.state.labels}
                                       onValueChange={this.handleChangeLabels}
                                       suggestions={suggests}/>
                <ReactQuill value={this.state.content} onChange={this.handleChangeContents}/>

                {this.state.edit?(
                    <Grid style={{width:'100%'}} container>
                        <Grid item xs={6}>

                        <Button fullWidth variant="contained" color="primary" style={{marginTop: 8}} onClick={this.onEdit}>
                            수정
                        </Button>
                        </Grid>
                        <Grid item xs={6}>
                        <Button fullWidth variant="contained" color="inherit" style={{marginTop: 8}} onClick={this.onDelete}>
                            삭제
                        </Button>
                        </Grid>
                    </Grid>):
                    (<Button fullWidth variant="contained" color="primary" style={{marginTop: 8}} onClick={this.onPost}>
                        등록
                    </Button>)}

            </Root>
        );
    }
}


MemoList.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MemoList);
