import axios from 'axios';

const ENDPOINT = process.env.REACT_APP_API_URL;
// const ENDPOINT = process.env.API_ENDPOINT ? process.env.API_ENDPOINT : 'http://localhost:3333';
const datePaser = (item) => {
    item['updatedAt'] = new Date(item['updatedAt']);
    item['createdAt'] = new Date(item['createdAt']);
    return item;
};

class MemoApi {
    constructor() {

    }

    Memo = {
        //Memo
        List() {
            return axios.get(`${ENDPOINT}/memos`).then((response) =>
                response.data.map((item) => datePaser(item))
            )
        },

        ListFromLabel(label_id) {
            return axios.get(`${ENDPOINT}/labels/${label_id}`).then((response) => {
                    return response.data.memos.map((item) => datePaser(item))
                }
            )
        },

        Get(id) {
            return axios.get(`${ENDPOINT}/memos/${id}`).then((response) =>
                datePaser(response.data)
            )
        },

        Create(data, labelIds = []) {
            let datas = [];
            return axios.post(`${ENDPOINT}/memos`, data).then((response) => {
                    datas = datePaser(response.data);
                    return datas
                }
            ).then(resp => {
                if (labelIds.length !== 0) {
                    let reqs = labelIds.map((i) =>
                        axios.post(`${ENDPOINT}/labels/${i}/memos`, {memoIds: [resp._id]})
                    );

                    return axios.all(reqs)
                }
            }).then(response => {
                return datas
            })
        },

        Update(id, data) {
            return axios.put(`${ENDPOINT}/memos/${id}`, data).then((response) =>
                datePaser(response.data)
            );
        },

        Delete(id) {
            return axios.delete(`${ENDPOINT}/memos/${id}`).then((response) =>
                datePaser(response.data)
            );
        },
    };
    Label = {
        //Label
        List(populate = false) {
            return axios.get(`${ENDPOINT}/labels?populate=${populate}`).then((response) => {
                return response.data.map((item) => datePaser(item));
            });
        },

        Delete(labelId) {
            return axios.delete(`${ENDPOINT}/labels/${labelId}`).then((response) =>
                datePaser(response.data)
            );
        },

        DeleteMemofromLabel(labelId, memoIds) {
            return axios.delete(`${ENDPOINT}/labels/${labelId}/memos`, {data: {memoIds: memoIds}}).then((response) =>
                datePaser(response.data)
            );
        },

        Create(title) {
            return axios.post(`${ENDPOINT}/labels`, {title: title}).then((response) =>
                datePaser(response.data)
            );
        },
    }
}

export default MemoApi;