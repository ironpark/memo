import _ from "lodash";
import React from "react";
import { Route } from 'react-router-dom';

const fnCheck = (key)=>{
    return key.indexOf('fn$') !== -1
};

const deep = (obj,obj2) => {
    let deepCopy = _.cloneDeep(obj);
    _.forEach(obj2, (value, key) => {
        if(fnCheck(key)){
            const k = key.split('fn$')[1];
            deepCopy[k] = value(deepCopy[k])
        }else{
            deepCopy[key] = obj2[key];
        }
    });
    return deepCopy;
};

//React.Component.setState 를 보다 쉽게 사용하기 위함.
export function customSetState(target) {
    // override setState function for partial update && immutability
    // special features fn$<key> : (e) => {}
    target.setState = (obj) => {
        let n = {};
        _.forEach(obj,(value,key)=>{
            const k = key.split('fn$')[1];
            if(fnCheck(key)){
                //함수 키
                n[k] = value(target.state[k])
            }else{
                //일반
                if(typeof obj[key] === 'object' && !Array.isArray(obj[key]) && !(obj[key] instanceof Map)) {
                    n[key] = deep(target.state[key], obj[key]);
                }else{
                    n[key] = obj[key];
                }
            }
        });
        React.Component.prototype.setState.call(target,n);
    };
}

const renderMergedProps = (component, ...rest) => {
    const finalProps = Object.assign({}, ...rest);
    return (
        React.createElement(component, finalProps)
    );
};

// Route에 props를 전달하기위한 custom route
export const PropsRoute = ({ component, ...rest }) => {
    return (
        <Route {...rest} render={routeProps => {
            return renderMergedProps(component, routeProps, rest);
        }}/>
    );
};

export default customSetState