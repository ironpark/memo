import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import { Button, Welcome } from '@storybook/react/demo';
import  MemoList  from '../container/MemoList';
import  MemoListItem  from '../components/MemoListItem';
import  MemoListToobar from '../components/MemoListToolbar';
import { withInfo } from '@storybook/addon-info';

storiesOf('Welcome', module).add('to Storybook', () => <Welcome showApp={linkTo('Button')} />);

storiesOf('MemoList', module)
    .add('item',
        withInfo(``)(() =>
        <MemoListItem
            title={"Title example"}
            summary={'Summary example loooooooooog'}
            key={'???'}
            labels={['test label','remember','memo']}
            date={"1994-09-01"}
        />)
    )
    .add('toolbar',
        withInfo(``)(() =>
            <MemoListToobar/>)
        )
    .add('list',
        withInfo(``)(() =>
            <MemoList
                items={[
                    {
                        title:"Title - 1",
                        summary:'sad',
                        labels:['test label', 'remember', 'memo'],
                        date:"1994-09-01"
                    },
                    {
                        title:"Title - 2",
                        summary:'sad',
                        labels:['test label', 'remember', 'memo','memo','memo'],
                        date:"1994-09-01"
                    },
                ]}
            />)
    );

